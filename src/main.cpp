//https://gist.github.com/akosma/864b886f992de462368a3f3e81595b7
//https://www.mobilefish.com/services

#include <mbed.h>

#include <mini-gmp.h>
#include <stdio.h>
#include <assert.h>



//SETUP
Serial pc(USBTX, USBRX); // enable serial communication, tx, rx
DigitalOut flashPin(LED1); //flash on D13


//VARIABLES
typedef struct binstr{ //struct for storing binary representations of numbers, including length
    int len;
    char* bin;
} binstr;

Timer t; //timer for measuring execution durartion
Timeout flasher; //delay for flash
bool keysize_2048bit = false;
bool counterMeasure = false;


//PROTOTYPE FUNCTIONS
void square_and_multiply( mpz_t res, mpz_t x, mpz_t k, mpz_t n);
void mpz2bin(binstr *s, mpz_t k);
void flashON();
void flashOFF();


//FUNCTIONS

/*function to convert mpz_t to binary string */
void mpz2bin(binstr *s, mpz_t k) {
    s->len = (int) mpz_sizeinbase(k, 2) + 2; //get length of k in binary representation, add 2 for sign and terminator;
    s->bin = (char *) malloc((s->len) * sizeof(char)); // initialize string to store binary representation of k
    mpz_get_str(s->bin, 2, k); // convert k to binary representation
}


/* function to efficiently calculate x^k mod n, using the square and multiply algorithm */
void square_and_multiply( mpz_t res, mpz_t x, mpz_t k, mpz_t n, binstr *s){

    mpz_set_ui(res,1); //set result = 1

    for (int i = 0; i < s->len-2 ; i++) { //iterate over the string
        //printf ("b_[%d] = %d\n",i,b[i] - '0');

        // result^2 mod n
        mpz_powm_ui(res, res, 2, n);
        if ((int) (s->bin[i] - '0') == 1) {
            // multiply result mod n
            mpz_mul(res, res, x);
            mpz_mod(res, res, n);
        }
    }
}

/* function to trigger flash */
void flashON(){
  flashPin = 1; //fire flash
}

/* function to to turn flash off */
void flashOFF(){
  flashPin = 0; //strobe off

}


int main(){
  pc.baud (115200); //set baud rate for serial communication

    mpz_t p;
    mpz_t p_minus;
    mpz_t q;
    mpz_t q_minus;
    mpz_t e;
    mpz_t n;
    mpz_t phi;
    mpz_t d;
    mpz_t gcd;
    mpz_t res;
    mpz_t message;
    mpz_t c;
    mpz_t s;
    mpz_t dp;
    mpz_t dq;
    mpz_t u;
    mpz_t v;
    mpz_t g;
    mpz_t sp;
    mpz_t sq;
    mpz_t diff;
    mpz_t cracked_prime;

    int flag_p;
    int flag_q;

    //char inmessage[512];
    int counter = 0;


    /* initialization of variables */
    mpz_init_set_ui(p,0); //first prime
    mpz_init_set_ui(p_minus,0); //p-1
    mpz_init_set_ui(q,0); //second prime
    mpz_init_set_ui(q_minus, 0); //q-1
    mpz_init_set_ui(e,65537); //public key
    mpz_init_set_ui(n,0); //public key
    mpz_init_set_ui(phi,0); //phi(n) = (p-1)*(q-1)
    mpz_init_set_ui(d,0); //private key
    mpz_init_set_ui(gcd,0); //temporary for storing GCD
    mpz_init_set_ui(res,0); //temporary for storing results
    mpz_init_set_ui(c,0); //encrypted message (cyphertext)
    mpz_init_set_ui(s,0); //decrypted message (signature)
    mpz_init_set_ui(g,1); //g=1, for CRT
    mpz_init_set_ui(dp,0); //dp = d mod (p-1), used for CRT
    mpz_init_set_ui(dq,0); //dq = d mod (q-1), used for CRT
    mpz_init_set_ui(u,0); //temporary for CRT
    mpz_init_set_ui(v,0); //temporary for CRT
    mpz_init_set_ui(sp,0); //part of signature
    mpz_init_set_ui(sq,0); //part of signature
    mpz_init_set_ui(diff,0); //differnce for calculating prime from wrong Signature
    mpz_init_set_ui(cracked_prime,0);

    mpz_init_set_str(message, "123456", 10); //message to be encrypted

    binstr encrypt; //struct for storing binary representation message for encryption
    binstr decrypt_p; //struct for storing binary representation for decryption
    binstr decrypt_q; //struct for storing binary representation for decryption


    mpz2bin(&encrypt, e); //convert encryption exponent e to binary string

    /*
    printf("Enter message to be encrypted (digits only!)\n");
    scanf("%s", &inmessage);
    mpz_set_str(message, inmessage, 10); //convert input message string to mpz_t
    */

    /* print message when program starts, additionally used to identify resets */
    printf("RESET!\t");
    printf("Original message = ");
    mpz_out_str(stdout, 10, message);
    printf("\t");


    t.start();

    /* assign pre-generated primes */
    if (!keysize_2048bit){
      flag_p = mpz_set_str(p, "11417804448676103073481812780848700555211344992137631887787123193410016962511245464526468027817487551225334052219354703859217112709260684426881105715831167", 10);
      assert (flag_p == 0); /* If flag is not 0 then the operation failed */

      flag_q = mpz_set_str(q, "8132374811556731151148927513605009044459260012340164197837572443508963939066246544552326502375144551707242665419044770237385350307952884268599245129482781", 10);
      assert (flag_q == 0); /* If flag is not 0 then the operation failed */
    }
    else{
      flag_p = mpz_set_str(p, "139638318435503642205293789831971578055818237747462704217712323213341459591808090038479174937965831154564496186044844142597900229587706504100503572225013979212187582890355368827185565157544506661350492859344398388117539697391721226912015964408257124008521622924517904124850913779517179741274385453400762338693", 10);
      assert (flag_p == 0); /* If flag is not 0 then the operation failed */

      flag_q = mpz_set_str(q, "138185521284249854821087890322183693601070461350171249895394461248715076934143345084861436417679362588234970424390098569618010503018796088837909259189513668846793783772218220414345815985095509459814079384583311309309786950737010897517654436352020264864095901962819577560654343251548252141470914340767557651259", 10);
      assert (flag_q == 0); /* If flag is not 0 then the operation failed */
    }

    /* compute gcd of primes to ensure they are teilerfremd */
    mpz_gcd(gcd,p,q);
    assert (mpz_cmp_ui(gcd, 1) == 0); // test if gcd = 1

    /* calculate needed variables */
    mpz_mul(n,p,q); //n = p * q

    mpz_sub_ui(p_minus,p,1);//p = p-1

    mpz_sub_ui(q_minus,q,1);//q = q-1

    mpz_mul(phi,p_minus,q_minus); /* phi = (q-1) * (p-1)*/


    mpz_invert(d,e,phi); //e*d mod(phi) = 1 -> d = 1/e


    /*
    ####################
    ENCRYPTION*/
    square_and_multiply(c, message, e, n, &encrypt);
    t.stop();
    printf("Encryption took %d milliseconds\n", t.read_ms());
    t.reset();



    /*
    ####################
    CRT DECRYPTION*/
    mpz_t tmp1;
    mpz_init_set_ui(tmp1,0);
    mpz_t tmp2;
    mpz_init_set_ui(tmp2,0);

    mpz_mod(dp, d, p_minus); //dp = d mod (p-1)
    mpz_mod(dq, d, q_minus); //dq = d mod (q-1)

    mpz2bin(&decrypt_p, dp); //convert encryption exponent dp to binary string
    mpz2bin(&decrypt_q, dq); //convert encryption exponent dq to binary string

    mpz_gcdext(g, u, v, p, q); //1 = p*u + q*v


    while(1){

    counter++;
    t.start(); //start timer

    flasher.attach(&flashON, 0.25); //keep pin high for Xs
    //flashON(); //start strobe

    square_and_multiply(sp, c, dp, p, &decrypt_p); //sp = c^dp mod p
    square_and_multiply(sq, c, dq, q, &decrypt_q); //sq = c^dq mod q
    flashOFF();

    //s = u*p*sp + v*q*sp mod n
    mpz_mul(tmp1, u, p);
    mpz_mul(tmp1, tmp1, sq);
    mpz_mul(tmp2, v, q);
    mpz_mul(tmp2, tmp2, sp);
    mpz_add(tmp1, tmp1, tmp2);
    mpz_mod(s, tmp1, n);

    t.stop();

    printf("This is run #%03d\t", counter);

    if (mpz_cmp(message,s) == 0){ //test if decrypted message is equal to original message
      printf("Decryption successful! It took %dms\t\t", t.read_ms());
      printf ("Signature = ");
      mpz_out_str(stdout, 10, s);
      printf("\n");
    }
    else{ //if decrypted message differs from original message decryption must have failed => do NOT print encrypted message

      if( counterMeasure) { //counter measure is to block output
      printf("WARNING: Decryption FAILED! Output BLOCKED!\n");
      }
      else{
        printf("Decryption FAILED! It took %dms\t\t", t.read_ms());
        printf ("Signature = "); //print the wrong signature
        mpz_out_str(stdout, 10, s);
        printf("\n");
        /* calculate the primes from the wrong signature*/
        mpz_sub(diff, message, s); //difference between original message and corrupted signature
        mpz_mod(diff, diff, n); //mod n on differnce to shorten
        mpz_gcd(cracked_prime, diff,n); //GCD of differnece and n is one of the primes

        printf ("A prime was cracked!\nIt is:");
        mpz_out_str(stdout, 10, s);
        printf("\n");

        if(mpz_cmp(cracked_prime, p) == 0){
          printf("Cracked prime is p!\n");
        }
        else if(mpz_cmp(cracked_prime, q) == 0){
            printf("Cracked prime is q!\n");
        }
      }
    }

    t.reset();
    }

    mpz_clear(tmp1);
    mpz_clear(tmp2);


    /*clean up the mpz_t handles */
    mpz_clear(p);
    mpz_clear(p_minus);
    mpz_clear(q);
    mpz_clear(q_minus);
    mpz_clear(e);
    mpz_clear(n);
    mpz_clear(phi);
    mpz_clear(d);
    mpz_clear(gcd);
    mpz_clear(res);
    mpz_clear(message);
    mpz_clear(c);
    mpz_clear(s);
    mpz_clear(dp);
    mpz_clear(dq);
    mpz_clear(u);
    mpz_clear(v);
    mpz_clear(g);
    mpz_clear(sp);
    mpz_clear(sq);
    mpz_clear(diff);
    mpz_clear(cracked_prime);

}
